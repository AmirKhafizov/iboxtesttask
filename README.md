1) Склонировать репозиторий 
git clone https://gitlab.com/AmirKhafizov/iboxtesttask.git
2) Добавить в папку resources файл application.properties,
скопировать содержимоё файла application.properties-dist
3) Создать базу данных с названием iBOXTestTask и прикрепить её к проекту
4) В файле application.properties в строке 
spring.datasource.password= заполнить пароль к бд
5) запустить проект в классе /ru/iBOX/application/Application.java
6) перейти по адресу localhost:8080/swagger-ui.html#/, где будет
доступна API. 