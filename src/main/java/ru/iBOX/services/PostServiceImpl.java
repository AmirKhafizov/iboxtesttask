package ru.iBOX.services;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.iBOX.exceptions.ForbiddenException;
import ru.iBOX.exceptions.NotFoundException;
import ru.iBOX.json.dto.PostDto;
import ru.iBOX.json.forms.CreatePostForm;
import ru.iBOX.json.forms.UpdatePostForm;
import ru.iBOX.json.mappers.CommentsMapper;
import ru.iBOX.models.Comments;
import ru.iBOX.models.Post;
import ru.iBOX.models.User;
import ru.iBOX.repositories.PostRepository;
import ru.iBOX.security.utils.SecurityUtils;



@Service("postService")
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private CommentsMapper commentsMapper;

    @Override
    public Post get(Long id) {
        return postRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Post with id = " + id + " not found."));
    }

    @Override
    public Post create(User user, CreatePostForm form) {
        Post post = Post.builder()
                .header(form.getHeader())
                .shortDescription(form.getShortDescription())
                .description(form.getDescription())
                .postOwner(user)
                .build();
        return postRepository.save(post);
    }

    @Override
    public Post update(User user, UpdatePostForm form, Long postId) {
        Post post = get(postId);
        if(!SecurityUtils.isPostOwnerOrAdmin(user,post)){
            throw new ForbiddenException("The user is not admin or post owner ");
        }
        if (StringUtils.isNotBlank(form.getHeader())){
            post.setHeader(form.getHeader());
        }
        post.setShortDescription(form.getShortDescription());
        post.setDescription(form.getDescription());

        return postRepository.save(post);
    }

    @Override
    public void delete(User user, Long postId) {
        if(!SecurityUtils.isSAdmin(user)){
            throw ForbiddenException.forDeletingPost();
        }
        Post post = get(postId);
        postRepository.delete(post);

    }

    @Override
    public Page<Post> getAllPosts(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    @Override
    public Page<Post> getAllPostsByUserId(Long userId, Pageable pageable) {
        return postRepository.findAllByPostOwnerId(userId, pageable);
    }

    @Override
    public Page<Post> getAllPostsByHeader(String header, Pageable pageable) {
        return postRepository.findAllByHeader(header, pageable);
    }

    @Override
    public Page<PostDto> setPageCommentsDto(Page<PostDto> postDtos, Pageable pageable) {
        for (PostDto postDto : postDtos){
            Page<Comments> comments = commentsService.getAllCommentByPostId(postDto.getId() ,pageable);
            postDto.setCommentsDtoList(commentsMapper.map(comments));
        }
        return postDtos;
    }
}
