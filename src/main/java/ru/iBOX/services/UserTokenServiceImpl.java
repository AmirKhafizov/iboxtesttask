package ru.iBOX.services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iBOX.application.AppConstants;
import ru.iBOX.application.configs.JwtConfig;
import ru.iBOX.exceptions.ForbiddenException;
import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.models.User;
import ru.iBOX.models.UserToken;
import ru.iBOX.repositories.UserRepository;
import ru.iBOX.repositories.UserTokenRepository;
import ru.iBOX.security.enums.TokenType;

import java.time.LocalDateTime;

import static ru.iBOX.utils.DateUtils.fromLocalDateTimeToMillis;

@Service
public class UserTokenServiceImpl implements UserTokenService {

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private UserTokenRepository userTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Override
    public UserToken getOrCreateTokenIfNotExists(User user) {
        UserToken userToken = userTokenRepository.findByUserId(user.getId());
        if (userToken != null) return userToken;

        userToken = createUserToken(user);
        return userTokenRepository.save(userToken);
    }

    @Override
    public TokenDto buildTokenDtoByUser(User user, UserToken userToken) {
        return TokenDto.builder()
                .userId(user.getId())
                .refreshToken(userToken.getRefreshToken())
                .accessToken(getTokenValue(user, TokenType.ACCESS))
                .refreshTokenExpiresAt(fromLocalDateTimeToMillis(userToken.getRefreshTokenExpiresAt()))
                .accessTokenExpiresAt(fromLocalDateTimeToMillis(userToken.getAccessTokenExpiresAt()))
                .role(user.getRole().toString())
                .build();
    }

    /**
     * builds token value
     *
     * @param user - user get token value for
     * @param type - type of token
     */
    @Override
    public String getTokenValue(User user, TokenType type) {
        return Jwts.builder()
                .claim("role", user.getRole().toString())
                .claim("email", StringUtils.isNotBlank(user.getEmail()))
                .claim("tokenParam", TokenType.REFRESH.equals(type) ? user.getRefreshTokenParam() : user.getAccessTokenParam())
                .claim("token-type", type.toString())
                .setSubject(user.getId().toString())
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret()).compact();
    }

    /**
     * updates access token
     *
     * @param user - user whose access token to update
     * @return tokenDto - dto data with tokens
     */
    @Override
    @Transactional
    public TokenDto updateAccessToken(User user) {
        UserToken userToken = getByUserId(user.getId());

        if (userTokenExpired(userToken, true))
            throw new ForbiddenException("Refresh token has expired");

        userToken.setAccessTokenExpiresAt(LocalDateTime.now().plusHours(AppConstants.ACCESS_TOKEN_EXPIRING_TIME_IN_HOURS));
        userTokenRepository.save(userToken);
        return buildTokenDtoByUser(updateAccessTokenParam(user), userToken);
    }

    /**
     * updates refresh token if it has expired
     */
    @Override
    public void updateRefreshToken(User user, UserToken userToken) {
        if (userTokenExpired(userToken, true)) {
            user.setRefreshTokenParam(new RandomString().nextString());
            userRepository.save(user);
        }
    }

    /**
     * checks if the token has expired
     *
     * @param token          to check
     * @param isRefreshToken type of token
     * @return true if token has expired
     */
    @Override
    public boolean userTokenExpired(UserToken token, boolean isRefreshToken) {
        if (isRefreshToken)
            return LocalDateTime.now().isAfter(token.getRefreshTokenExpiresAt());
        else
            return LocalDateTime.now().isAfter(token.getAccessTokenExpiresAt());
    }

    @Override
    public UserToken getByUserId(Long userId) {
        return userTokenRepository.findByUserId(userId);
    }

    @Override
    public TokenDto getTokenByUser(User user) {
        UserToken token = getOrCreateTokenIfNotExists(user);
        return buildTokenDtoByUser(user, token);
    }

    private UserToken createUserToken(User user) {
        return UserToken.builder()
                .user(user)
                .accessTokenExpiresAt(LocalDateTime.now().plusHours(AppConstants.ACCESS_TOKEN_EXPIRING_TIME_IN_HOURS))
                .refreshTokenExpiresAt(LocalDateTime.now().plusDays(AppConstants.REFRESH_TOKEN_EXPIRING_TIME_IN_DAYS))
                .refreshToken(getTokenValue(user, TokenType.REFRESH))
                .build();
    }

    private User updateAccessTokenParam(User user) {
        User updateAccessTokenUser = userService.get(user.getId());
        updateAccessTokenUser.setAccessTokenParam(new RandomString().nextString());
        updateAccessTokenUser = userRepository.save(updateAccessTokenUser);
        return updateAccessTokenUser;
    }
}
