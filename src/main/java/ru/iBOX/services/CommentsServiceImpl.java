package ru.iBOX.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.iBOX.exceptions.ForbiddenException;
import ru.iBOX.exceptions.NotFoundException;
import ru.iBOX.json.forms.CreateCommentsForm;
import ru.iBOX.json.forms.UpdateCommentsForm;
import ru.iBOX.models.Comments;
import ru.iBOX.models.User;
import ru.iBOX.repositories.CommentsRepository;
import ru.iBOX.security.utils.SecurityUtils;

@Service("commentsService")
public class CommentsServiceImpl implements CommentsService {
    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private UserService userService;

    @Autowired PostService postService;

    @Override
    public Comments get(Long id) {
        return commentsRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Comment with id = " + id + " not found."));
    }

    @Override
    public Comments create(User user, CreateCommentsForm form, Long postId) {
        Comments comments = Comments.builder()
                .content(form.getContent())
                .commentOwner(userService.get(form.getCommentOwnerId()))
                .post(postService.get(postId))
                .build();
        return commentsRepository.save(comments);
    }

    @Override
    public Comments update(User user, UpdateCommentsForm form, Long commentId) {
        Comments comment = get(commentId);
        if (!SecurityUtils.isCommentOwnerOrAdmin(user, comment)){
            throw ForbiddenException.forUpdatingComment();
        }
        comment.setContent(form.getContent());
        comment.setCommentOwner(userService.get(form.getCommentOwnerId()));
        return commentsRepository.save(comment);
    }

    @Override
    public void delete(User user, Long commentId) {
        Comments comment = get(commentId);
        commentsRepository.delete(comment);
    }

    @Override
    public Page<Comments> getAllCommentByPostId(Long postId, Pageable pageable) {
        return commentsRepository.findAllByPostId(postId, pageable);
    }
}
