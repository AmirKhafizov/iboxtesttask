package ru.iBOX.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.iBOX.json.dto.PostDto;
import ru.iBOX.json.forms.CreatePostForm;
import ru.iBOX.json.forms.UpdatePostForm;
import ru.iBOX.models.Post;
import ru.iBOX.models.User;


public interface PostService extends RetrieveService<Post> {
    Post create(User user, CreatePostForm form);
    Post update(User user, UpdatePostForm form, Long postId);
    void delete(User user, Long postId);
    Page<Post> getAllPosts(Pageable pageable);
    Page<Post> getAllPostsByUserId(Long userId, Pageable pageable);
    Page<Post> getAllPostsByHeader(String header, Pageable pageable);
    Page<PostDto> setPageCommentsDto(Page<PostDto> postDtos, Pageable pageable);
}
