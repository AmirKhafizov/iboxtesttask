package ru.iBOX.services;

import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.json.forms.ChangePasswordForm;
import ru.iBOX.json.forms.UserLoginForm;
import ru.iBOX.json.forms.UserRegistrationForm;
import ru.iBOX.json.forms.UserUpdateForAdminForm;
import ru.iBOX.models.User;

public interface UserService extends RetrieveService<User> {
    TokenDto register(UserRegistrationForm form);
    TokenDto login(UserLoginForm form);
    User get(String email);
    User updateForAdmin(User user, UserUpdateForAdminForm form, Long userId);
    void changePassword(User user, ChangePasswordForm changePasswordForm);
    void deleteUser(User user, Long id);
}
