package ru.iBOX.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.iBOX.json.forms.CreateCommentsForm;
import ru.iBOX.json.forms.UpdateCommentsForm;
import ru.iBOX.models.Comments;
import ru.iBOX.models.User;

public interface CommentsService extends RetrieveService<Comments> {
    Comments create(User user, CreateCommentsForm form, Long postId);
    Comments update(User user, UpdateCommentsForm form, Long commentId);
    void delete(User user, Long commentId);
    Page<Comments> getAllCommentByPostId(Long postId, Pageable pageable);
}
