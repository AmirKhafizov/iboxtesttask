package ru.iBOX.services;

import com.google.common.collect.Lists;

import java.util.List;

public interface RetrieveService<T> {
    T get(Long id);
    default List<T> getAll() {
        return Lists.newArrayList();
    }

}
