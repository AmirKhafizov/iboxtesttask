package ru.iBOX.services;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iBOX.application.HttpResponse;
import ru.iBOX.exceptions.BadRequestException;
import ru.iBOX.exceptions.ForbiddenException;
import ru.iBOX.exceptions.NotFoundException;
import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.json.forms.ChangePasswordForm;
import ru.iBOX.json.forms.UserLoginForm;
import ru.iBOX.json.forms.UserRegistrationForm;
import ru.iBOX.json.forms.UserUpdateForAdminForm;
import ru.iBOX.models.User;
import ru.iBOX.models.UserToken;
import ru.iBOX.repositories.UserRepository;
import ru.iBOX.repositories.UserTokenRepository;
import ru.iBOX.security.enums.Role;
import ru.iBOX.security.utils.PasswordUtils;
import ru.iBOX.security.utils.SecurityUtils;
import ru.iBOX.utils.RandomStringUtils;


import static java.util.regex.Pattern.matches;
import static ru.iBOX.application.Patterns.VALID_EMAIL_REGEX;
import static ru.iBOX.utils.DataFormatter.formatEmail;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserTokenService userTokenService;

    @Autowired
    private UserTokenRepository userTokenRepository;

    @Autowired
    private PasswordUtils passwordUtils;

    @Override
    public TokenDto register(UserRegistrationForm form){
        String email;
        if (StringUtils.isBlank(form.getEmail())) {
            throw new BadRequestException("Login must not be null");
        }
        email = form.getEmail();
        throwIfExistsByEmail(email);

        User user = get(form);
        user.setRole(Role.CLIENT);
        user = userRepository.save(user);
        return userTokenService.getTokenByUser(user);
    };

    @Override
    public TokenDto login(UserLoginForm form) {
        String email;
        if (StringUtils.isNotBlank(form.getEmail())) {
            if (!matches(VALID_EMAIL_REGEX, form.getEmail()))
                throw new BadRequestException(HttpResponse.BAD_EMAIL);
            email = form.getEmail();
        } else throw new ForbiddenException("Email must not be null");
        String password = form.getPassword();
        User user = get(email);

        if (passwordUtils.matches(password, user.getHashedPassword())) {
            UserToken token = userTokenService.getByUserId(user.getId());
            if (token == null) {
                token = userTokenService.getOrCreateTokenIfNotExists(user);
            }
            userTokenService.updateRefreshToken(user, token);
            return userTokenService.buildTokenDtoByUser(user, token);
        } else throw new ForbiddenException("Email/Password is incorrect");
    }

    @Override
    public User get(String email) {
        return userRepository.findByEmail(email).orElseThrow(() ->
                new ForbiddenException(new NotFoundException("User with email = " + email + " not found.").getMessage()));
    }

    @Override
    public User get(Long id) {
        return userRepository.findById(id).orElseThrow(() ->
                new NotFoundException("User with id = " + id + " not found."));
    }

    protected User get(UserRegistrationForm form) {
        User user = User.builder()
                .hashedPassword(passwordUtils.hash(form.getPassword()))
                .accessTokenParam(new RandomStringUtils().nextString())
                .refreshTokenParam(new RandomStringUtils().nextString())
                .build();
        if (StringUtils.isNotBlank(form.getEmail())) {
            user.setEmail(formatEmail(form.getEmail()));
        } else throw new BadRequestException("Email must be not null");
        return user;
    }

    @Override
    public User updateForAdmin(User admin, UserUpdateForAdminForm form, Long userId) {
        if(!SecurityUtils.isSAdmin(admin)){
            throw ForbiddenException.forUpdatingUserByAdmin();
        }
        User user = get(userId);
        if (StringUtils.isNotBlank(form.getEmail()) && matches(VALID_EMAIL_REGEX, form.getEmail())) {
            user.setEmail(formatEmail(form.getEmail()));
        } else throw new BadRequestException("Email is invalid or empty");
        user.setHashedPassword(passwordUtils.hash(form.getNewPassword()));
        return userRepository.save(user);
    }

    public void changePassword(User user, ChangePasswordForm changePasswordForm){
        user = userRepository
                .findById(user.getId())
                .orElseThrow(NotFoundException::new);

        if (!passwordUtils.matches(changePasswordForm.getOldPassword(), user.getHashedPassword())){
            throw new ForbiddenException("Неверный пароль");
        }

        user.setHashedPassword(passwordUtils.hash(changePasswordForm.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void deleteUser(User user, Long id) {
        if(!SecurityUtils.isSAdmin(user)){
            throw ForbiddenException.forDeletingUser();
        }
        userTokenRepository.deleteByUserId(id);
        userRepository.deleteById(id);
    }

    private Boolean existsByEmail(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    protected void throwIfExistsByEmail(String login) {
        if (existsByEmail(login))
            throw new BadRequestException("Пользователь с таким email уже существует");
    }
}
