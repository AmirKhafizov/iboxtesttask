package ru.iBOX.services;

import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.models.User;
import ru.iBOX.models.UserToken;
import ru.iBOX.security.enums.TokenType;

public interface UserTokenService {
    UserToken getOrCreateTokenIfNotExists(User user);
    TokenDto buildTokenDtoByUser(User user, UserToken userToken);
    String getTokenValue(User user, TokenType type);
    TokenDto updateAccessToken(User user);
    void updateRefreshToken(User user, UserToken userToken);
    boolean userTokenExpired(UserToken token, boolean isRefreshToken);
    UserToken getByUserId(Long userId);
    TokenDto getTokenByUser(User user);
}
