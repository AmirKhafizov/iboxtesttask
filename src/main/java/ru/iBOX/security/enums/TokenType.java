package ru.iBOX.security.enums;

public enum TokenType {
    ACCESS, REFRESH
}
