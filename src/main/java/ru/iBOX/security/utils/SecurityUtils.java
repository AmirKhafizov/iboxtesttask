package ru.iBOX.security.utils;

import ru.iBOX.models.Comments;
import ru.iBOX.models.Post;
import ru.iBOX.models.User;
import ru.iBOX.security.enums.Role;

public class SecurityUtils {
    public static boolean isClient(User user) {
        return Role.CLIENT.equals(user.getRole());
    }

    public static boolean isSAdmin(User user) {
        return Role.ADMIN.equals(user.getRole());
    }

    public static boolean isPostOwnerOrAdmin(User user, Post post) {
        return post.getPostOwner().equals(user) || Role.ADMIN.equals(user.getRole());}
    public static boolean isCommentOwnerOrAdmin(User user, Comments comment){
        return comment.getCommentOwner().equals(user) || Role.ADMIN.equals((user.getRole()));
    }
}
