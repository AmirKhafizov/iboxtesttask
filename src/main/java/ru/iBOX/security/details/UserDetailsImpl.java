package ru.iBOX.security.details;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.iBOX.models.User;
import ru.iBOX.models.UserToken;
import ru.iBOX.security.enums.Role;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {

    private User user;
    private UserToken userToken;

    public UserDetailsImpl(User user) {
        this.user = user;
    }

    public UserDetailsImpl(Long id, String role, String email, UserToken userToken) {
        this.user = User.builder()
                .role(Role.valueOf(role))
                .email(email)
                .build();
        this.user.setId(id);
        this.userToken = userToken;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().toString());
        return Collections.singletonList(authority);
    }

    @Override
    public String getPassword() {
        return user.getHashedPassword();
    }

    @Override
    public String getUsername() {
        String email = user.getEmail();
        if (StringUtils.isNotBlank(email)) {
            return email;
        } else throw new IllegalStateException("Email is null");
    }

    @Override
    public boolean isAccountNonExpired() {
        LocalDateTime now = LocalDateTime.now();
        return now.isBefore(userToken.getAccessTokenExpiresAt());
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }
}
