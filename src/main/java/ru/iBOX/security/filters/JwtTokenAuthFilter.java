package ru.iBOX.security.filters;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.iBOX.application.configs.JwtConfig;
import ru.iBOX.controllers.UserTokenController;
import ru.iBOX.security.authentication.JwtTokenAuthentication;
import ru.iBOX.security.providers.JwtTokenAuthenticationProvider;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class JwtTokenAuthFilter implements Filter {

    @Autowired
    private JwtTokenAuthenticationProvider provider;

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String authorizationHeader = request.getHeader("Authorization");
        JwtTokenAuthentication authentication;


        SecurityContext context = SecurityContextHolder.getContext();
        if (authorizationHeader == null) {
            authentication = new JwtTokenAuthentication(null);
            authentication.setAuthenticated(false);
        } else  {
            String token = "";
            if (!StringUtils.isBlank(authorizationHeader)) {
                String prefix = jwtConfig.getPrefix() + " ";
                token = authorizationHeader.substring(prefix.length());
            }
            authentication = new JwtTokenAuthentication(token);
            if (request.getRequestURI().equals(UserTokenController.GET_NEW_ACCESS_TOKEN)) {
                authentication.setRefresh(true);
            }
            context.setAuthentication(provider.authenticate(authentication));

            if (!StringUtils.isBlank(context.getAuthentication().getName()) && !context.getAuthentication().isAuthenticated()) {
                String message;
                if (authentication.isRefresh()) {
                    message = "{\"message\": \"Refresh token has expired.\"}";
                } else {
                    message = "{\"message\": \"Access token has expired.\"}";
                }
                response.setContentLength(message.length());
                response.setContentType("application/json");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getOutputStream().print(message);
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
