package ru.iBOX.exceptions;

public class ForbiddenException extends RuntimeException {

    public ForbiddenException() {
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(Throwable cause) {
        super(cause);
    }

    public static ForbiddenException forDeletingUser() {
        throw new ForbiddenException("You don't have the necessary rights to delete user.");
    }
    public static ForbiddenException forUpdatingUserByAdmin() {
        throw new ForbiddenException("You don't have the necessary rights to update user.");
    }
    public static ForbiddenException forDeletingPost(){
        throw new ForbiddenException("You don't have the necessary rights to delete post.");
    }
    public static ForbiddenException forUpdatingComment(){
        throw new ForbiddenException("You don't have the necessary rights to update comment");
    }
}
