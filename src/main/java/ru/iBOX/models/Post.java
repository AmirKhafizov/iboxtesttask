package ru.iBOX.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "post")
public class Post extends AbstractPersistableEntity{
    @Column(nullable = false)
    private String header;

    @Column
    private String shortDescription;

    @Column
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User postOwner;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Comments> commentsList;
}
