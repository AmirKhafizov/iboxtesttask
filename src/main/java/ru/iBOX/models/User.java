package ru.iBOX.models;

import lombok.*;
import ru.iBOX.security.enums.Role;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "users")
public class User extends AbstractPersistableEntity{
    @Column(unique = true, nullable = false)
    private String email;

    @Column(name = "hashed_password", nullable = false)
    private String hashedPassword;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(unique = true)
    private String accessTokenParam;

    @Column(unique = true)
    private String refreshTokenParam;
}
