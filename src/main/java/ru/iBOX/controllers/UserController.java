package ru.iBOX.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.json.dto.UserDto;
import ru.iBOX.json.forms.UserRegistrationForm;
import ru.iBOX.json.forms.UserUpdateForAdminForm;
import ru.iBOX.json.mappers.UserMapper;
import ru.iBOX.models.User;
import ru.iBOX.services.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping(UserController.ROOT)
@Api(tags = "User Controller")
public class UserController {
    public static final String ROOT = "/users";

    private static final String REGISTER = "/register";
    private static final String ONE = "/{id}";
    private static final String CHANGE_PASSWORD = "/changePassword";

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @PostMapping(REGISTER)
    @ApiOperation("Register a new  user")
    public ResponseEntity<TokenDto> register(@RequestBody @Valid UserRegistrationForm form) {
        TokenDto tokenDto = userService.register(form);
        return ResponseEntity.ok(tokenDto);
    }

    @GetMapping(ONE)
    @ApiOperation("Get a user's information")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = false, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserDto> getUser(@PathVariable("id") Long userId,
                                          @AuthenticationPrincipal User user) {
        User profile = userService.get(userId);
        return ResponseEntity.ok(userMapper.map(profile));
    }

    @PutMapping(ONE)
    @ApiOperation("Update user's information")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<UserDto> updateForAdmin(@PathVariable("id") Long userId,
                                          @RequestBody @Valid UserUpdateForAdminForm form,
                                          @AuthenticationPrincipal User user) {
        User updated = userService.updateForAdmin(user, form, userId);
        return ResponseEntity.ok(userMapper.map(updated));
    }

    @DeleteMapping(ONE)
    @ApiOperation("Delete user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long userId,
                                           @AuthenticationPrincipal User user) {
        userService.deleteUser(user,userId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }
}
