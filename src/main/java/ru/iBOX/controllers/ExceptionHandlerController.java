package ru.iBOX.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.iBOX.exceptions.BadRequestException;
import ru.iBOX.exceptions.ForbiddenException;
import ru.iBOX.exceptions.NotFoundException;
import ru.iBOX.json.dto.ExceptionDto;


@ControllerAdvice
public class ExceptionHandlerController {
    @ResponseBody
    @ExceptionHandler({BadRequestException.class, IllegalArgumentException.class})
    public ResponseEntity<ExceptionDto> handleBadRequest(BadRequestException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ExceptionDto.of(ex.getMessage()));
    }

    @ResponseBody
    @ExceptionHandler({ForbiddenException.class})
    public ResponseEntity<ExceptionDto> handleForbidden(ForbiddenException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ExceptionDto.of(ex.getMessage()));
    }
    @ResponseBody
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ExceptionDto> handleNotFound(NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ExceptionDto.of(ex.getMessage()));
    }
}
