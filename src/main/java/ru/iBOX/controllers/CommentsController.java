package ru.iBOX.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.iBOX.json.dto.CommentsDto;
import ru.iBOX.json.dto.PostDto;
import ru.iBOX.json.forms.CreateCommentsForm;
import ru.iBOX.json.forms.CreatePostForm;
import ru.iBOX.json.forms.UpdateCommentsForm;
import ru.iBOX.json.mappers.CommentsMapper;
import ru.iBOX.models.Comments;
import ru.iBOX.models.Post;
import ru.iBOX.models.User;
import ru.iBOX.services.CommentsService;

import javax.validation.Valid;

@RestController
@RequestMapping(CommentsController.ROOT)
@Api(tags = "Comments Controller")
public class CommentsController {
    public static final String ROOT = "/comments";
    private static final String ONE = "/{commentId}";
    private static final String CREATE_COMMENT = "/create/post/{postId}";
    private static final String GET_ALL = "/all";
    private static final String GET_ALL_BY_POST_ID = GET_ALL + "/post/{id}";

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private CommentsMapper commentsMapper;

    @PutMapping(CREATE_COMMENT)
    @ApiOperation("Create comment")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    public ResponseEntity<CommentsDto> createComment(@RequestBody @Valid CreateCommentsForm form,
                                                     @AuthenticationPrincipal User user,
                                                     @PathVariable("postId") Long postId) {
        Comments comment = commentsService.create(user, form, postId);
        return ResponseEntity.ok(commentsMapper.map(comment));
    }

    @PutMapping(ONE)
    @ApiOperation("Update comment")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CommentsDto> updateComment(@RequestBody @Valid UpdateCommentsForm form,
                                                     @AuthenticationPrincipal User user,
                                                     @PathVariable("commentId") Long commentId) {
        Comments comment = commentsService.update(user, form, commentId);
        return ResponseEntity.ok(commentsMapper.map(comment));
    }

    @DeleteMapping(ONE)
    @ApiOperation("Delete comment")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<String> deleteComment(@AuthenticationPrincipal User user,
                                                @PathVariable("commentId") Long commentId) {
        commentsService.delete(user, commentId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }

    @GetMapping(GET_ALL_BY_POST_ID)
    @ApiOperation("Get all comments by post id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Page<CommentsDto>> getAllCommentsByPostId(@AuthenticationPrincipal User user,
                                                @PathVariable("id") Long id,
                                                         @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC)
                                                                     Pageable pageable) {
        Page<Comments> comments = commentsService.getAllCommentByPostId(id, pageable);
        return ResponseEntity.ok(commentsMapper.map(comments));
    }
}
