package ru.iBOX.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.iBOX.json.dto.PostDto;
import ru.iBOX.json.forms.CreatePostForm;
import ru.iBOX.json.forms.UpdatePostForm;
import ru.iBOX.json.mappers.CommentsMapper;
import ru.iBOX.json.mappers.PostMapper;
import ru.iBOX.models.Comments;
import ru.iBOX.models.Post;
import ru.iBOX.models.User;
import ru.iBOX.services.CommentsService;
import ru.iBOX.services.PostService;

import javax.validation.Valid;

@RestController
@RequestMapping(PostController.ROOT)
@Api(tags = "Post Controller")
public class PostController {
    public static final String ROOT = "/posts";
    private static final String CREATE_POST = "/create";
    private static final String ONE = "/{postId}";
    private static final String GET_ALL = "/all";
    private static final String GET_ALL_BY_USER_ID = GET_ALL + "/user/{userId}";
    private static final String GET_ALL_BY_HEADER = GET_ALL + "/header/{headerPost}";

    @Autowired
    private PostService postService;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private CommentsMapper commentsMapper;

    @PostMapping(CREATE_POST)
    @ApiOperation("Create post")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    public ResponseEntity<PostDto> createPost(@RequestBody @Valid CreatePostForm form,
                                              @AuthenticationPrincipal User user) {
        Post newPost = postService.create(user, form);
        return ResponseEntity.ok(postMapper.map(newPost));
    }

    @PutMapping(ONE)
    @ApiOperation("Update post")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAnyAuthority('CLIENT', 'ADMIN')")
    public ResponseEntity<PostDto> updatePost(@RequestBody @Valid UpdatePostForm form,
                                              @AuthenticationPrincipal User user,
                                              @PathVariable("postId") Long postId) {
        Post newPost = postService.update(user, form, postId);
        return ResponseEntity.ok(postMapper.map(newPost));
    }

    @DeleteMapping(ONE)
    @ApiOperation("Delete post")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<String> deletePost(@AuthenticationPrincipal User user,
                                              @PathVariable("postId") Long postId) {
        postService.delete(user, postId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }

    @GetMapping(GET_ALL)
    @ApiOperation("Get all posts")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = false, dataType = "string", paramType = "header")
    })
    public ResponseEntity<Page<PostDto>> getAllPosts(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC)
                                                         Pageable pageable) {
        Page<Post> posts = postService.getAllPosts(pageable);
        Page<PostDto> postDtos = postMapper.map(posts);
        postService.setPageCommentsDto(postDtos,pageable);
        return ResponseEntity.ok(postDtos);
    }

    @GetMapping(GET_ALL_BY_USER_ID)
    @ApiOperation("Get all posts by user id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = false, dataType = "string", paramType = "header")
    })
    public ResponseEntity<Page<PostDto>> getAllPostsByUserId(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC)
                                                         Pageable pageable,
                                                          @PathVariable("userId") Long userId) {
        Page<Post> posts = postService.getAllPostsByUserId(userId, pageable);
        Page<PostDto> postDtos = postMapper.map(posts);
        postService.setPageCommentsDto(postDtos,pageable);
        return ResponseEntity.ok(postDtos);
    }

    @GetMapping(GET_ALL_BY_HEADER)
    @ApiOperation("Get all posts by header")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = false, dataType = "string", paramType = "header")
    })
    public ResponseEntity<Page<PostDto>> getAllPostsByHeader(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC)
                                                                     Pageable pageable,
                                                             @PathVariable("headerPost") String headerPost) {
        Page<Post> posts = postService.getAllPostsByHeader(headerPost, pageable);
        Page<PostDto> postDtos = postMapper.map(posts);
        postService.setPageCommentsDto(postDtos,pageable);
        return ResponseEntity.ok(postDtos);
    }



}
