package ru.iBOX.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.iBOX.json.dto.TokenDto;
import ru.iBOX.models.User;
import ru.iBOX.services.UserTokenService;

@RestController
@Api("User Token Controller")
public class UserTokenController {

    public static final String GET_NEW_ACCESS_TOKEN = "/token/refresh";

    @Autowired
    private UserTokenService userTokenService;

    @PostMapping(GET_NEW_ACCESS_TOKEN)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization header", defaultValue = "Bearer %token%",
                    required = true, dataType = "string", paramType = "header")
    })
    @ApiOperation("Get access token using REFRESH TOKEN")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<TokenDto> getAccessToken(@AuthenticationPrincipal User user) {
        TokenDto tokenDto = userTokenService.updateAccessToken(user);
        return ResponseEntity.ok(tokenDto);
    }
}
