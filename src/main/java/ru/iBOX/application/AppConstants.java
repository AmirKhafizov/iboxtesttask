package ru.iBOX.application;

public interface AppConstants {
    Integer ACCESS_TOKEN_EXPIRING_TIME_IN_HOURS = 4;
    Integer REFRESH_TOKEN_EXPIRING_TIME_IN_DAYS = 180;
}
