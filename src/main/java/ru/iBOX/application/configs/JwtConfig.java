package ru.iBOX.application.configs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "jwt")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtConfig {
    private String secret;
    private String header;
    private String prefix;
}
