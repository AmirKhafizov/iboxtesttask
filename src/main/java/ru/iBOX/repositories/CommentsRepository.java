package ru.iBOX.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.iBOX.models.Comments;

public interface CommentsRepository extends JpaRepository<Comments, Long> {
    Page<Comments> findAll(Pageable pageable);
    Page<Comments> findAllByPostId(Long postId, Pageable pageable);
}
