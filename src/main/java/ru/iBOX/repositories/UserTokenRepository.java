package ru.iBOX.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iBOX.models.UserToken;

public interface UserTokenRepository extends JpaRepository<UserToken, Long> {
    UserToken findByUserId(Long id);
    void deleteByUserId(Long id);
}
