package ru.iBOX.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.iBOX.models.Post;


public interface PostRepository extends JpaRepository<Post, Long> {
    Page<Post> findAll(Pageable pageable);
    Page<Post> findAllByPostOwnerId(Long userId, Pageable pageable);
    Page<Post> findAllByHeader(String header, Pageable pageable);
}
