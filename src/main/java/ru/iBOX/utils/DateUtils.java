package ru.iBOX.utils;

import java.time.*;

public class DateUtils {

    public static LocalDate dateFromLong(Long millis) {
        if (millis == null)
            return null;
        return Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static String localDateToString(LocalDate localDate) {
        if (localDate == null)
            return null;
        return localDate.getYear() + "-" + localDate.getMonthValue() + "-" + localDate.getDayOfMonth();
    }

    public static String millisToDateString(Long millis) {
        LocalDate localDate = dateFromLong(millis);
        return localDateToString(localDate);
    }

    public static Long fromLocalDateTimeToMillis(LocalDateTime localDateTime) {
        if (localDateTime == null) return null;
        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        return zdt.toInstant().toEpochMilli();
    }

    public static LocalDateTime millsToLocalDateTime(Long millis) {
        Instant instant = Instant.ofEpochMilli(millis);
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDate millisToLocalDate(Long millis) {
        return Instant
                .ofEpochMilli(millis)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
