package ru.iBOX.json.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private Long id;

    private String header;

    private String shortDescription;

    private String description;

    private UserDto userDto;

    private Page<CommentsDto> commentsDtoList;
}
