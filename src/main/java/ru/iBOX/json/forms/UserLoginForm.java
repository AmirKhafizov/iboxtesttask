package ru.iBOX.json.forms;

import lombok.*;
import ru.iBOX.application.HttpResponse;
import ru.iBOX.application.Patterns;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserLoginForm {

    @NotNull
    @NotEmpty
    @Pattern(regexp = Patterns.VALID_EMAIL_REGEX, message = HttpResponse.BAD_EMAIL)
    private String email;

    @NotNull
    @NotEmpty
    private String password;
}
