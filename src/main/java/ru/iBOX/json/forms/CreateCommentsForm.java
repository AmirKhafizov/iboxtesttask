package ru.iBOX.json.forms;

import lombok.*;
import ru.iBOX.models.User;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCommentsForm {
    @NotNull
    @NotEmpty
    private String content;

    @NotNull
    private Long commentOwnerId;
}
