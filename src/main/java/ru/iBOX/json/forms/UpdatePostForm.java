package ru.iBOX.json.forms;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePostForm {
    @NotNull
    @NotEmpty
    private String header;

    private String shortDescription;

    private String Description;
}
