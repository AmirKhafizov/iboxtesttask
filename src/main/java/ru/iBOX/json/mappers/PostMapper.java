package ru.iBOX.json.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ru.iBOX.json.dto.PostDto;
import ru.iBOX.models.Post;

@Component
public class PostMapper implements Mapper<Post, PostDto> {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CommentsMapper commentsMapper;

    @Override
    public PostDto map(Post post) {
        if(post == null) return null;
        return PostDto.builder()
                .id(post.getId())
                .header(post.getHeader())
                .shortDescription(post.getShortDescription())
                .description(post.getDescription())
                .userDto(post.getPostOwner() != null ? userMapper.map(post.getPostOwner()) : null)
                .build();
    }

    public Page<PostDto> map(Page<Post> posts) {
        return posts.map(this::map);
    }
}
