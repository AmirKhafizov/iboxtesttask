package ru.iBOX.json.mappers;

import java.util.List;
import java.util.stream.Collectors;

/**
 * interface which converts domain model to json
 *
 * @implNote F - domain model to get json from
 * @implNote T - json data
 */
public interface Mapper<F, T> {
    T map(F f);

    default List<T> map(List<F> f) {
        return f.stream().map(this::map).collect(Collectors.toList());
    }
}
