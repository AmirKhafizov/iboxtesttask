package ru.iBOX.json.mappers;

import org.springframework.stereotype.Component;
import ru.iBOX.json.dto.UserDto;
import ru.iBOX.models.User;

@Component
public class UserMapper implements Mapper<User, UserDto> {
    @Override
    public UserDto map(User user) {
            if(user == null) return null;
            return UserDto.builder()
                    .id(user.getId())
                    .email(user.getEmail())
                    .role(user.getRole().toString())
                    .build();
    }
}
