package ru.iBOX.json.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import ru.iBOX.json.dto.CommentsDto;
import ru.iBOX.models.Comments;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommentsMapper implements Mapper<Comments, CommentsDto> {
    @Autowired
    private UserMapper userMapper;

    @Override
    public CommentsDto map(Comments comments) {
        if(comments == null) return null;
        return CommentsDto.builder()
                .id(comments.getId())
                .content(comments.getContent())
                .userDto(comments.getCommentOwner() != null ?  userMapper.map(comments.getCommentOwner()) : null)
                .build();
    }

    @Override
    public List<CommentsDto> map(List<Comments> comments) {
        if (comments == null){
            return null;
        }
        return comments.stream().map(this::map).collect(Collectors.toList());
    }

    public Page<CommentsDto> map(Page<Comments> comments){
        if (comments == null){
            return null;
        }
        return comments.map(this::map);
    }
}
